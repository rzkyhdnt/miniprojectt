package com.miniproject.miniproject.controller;

import com.miniproject.miniproject.model.Content;
import com.miniproject.miniproject.repository.ContentRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;


import org.springframework.web.bind.annotation.GetMapping;



@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/list")
public class GetController {
    @Autowired
    private ContentRepo contentRepo;

    @GetMapping("/content")
    public List<Content> listContent() {
        return contentRepo.findAll();
    }
    
}
