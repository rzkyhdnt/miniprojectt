package com.miniproject.miniproject.controller;

import com.miniproject.miniproject.model.Content;
import com.miniproject.miniproject.repository.ContentRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/save")
public class PostController {
    @Autowired
    ContentRepo contentRepo;

    @RequestMapping(value = "/content", consumes = "application/json")
    public Content saveContent(@RequestBody Content content){
        return contentRepo.save(content);
    }
}
